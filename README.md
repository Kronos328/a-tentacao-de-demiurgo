# A Tentação de Demiurgo
![Trailer do jogo](https://www.youtube.com/watch?v=pXJVNTuuGVE)

Uma build jogável pode ser acessada [aqui](https://medajax.itch.io/a-tentacao-de-schneider)

# Sinopse
E se você pudesse resolver seus problemas em um estalar de dedos?

O Demiurgo aparece na sua frente e lhe faz uma oferta, a possibilidade de solucionar todas as dificuldades de sua vida com um simples estalar de dedos.

Mas lembre-se: suas escolhas podem acabar te transformar em seu próprio inimigo.

Nesse plataformer imersivo, você deve alcançar o topo da ruínas, indo da forma mais difícil ou aproveitando a bênção do Demiurgo e seguindo o caminho mais fácil.


# Controles

WASD  ->  Movimentação do personagem.

Barra de Espaço -> Pular.

Shift  -> Correr.

Botão Direito -> Facilita o caminho do jogador ao retirar todos os obstáculos do mapa durante alguns segundos.

Botão Esquerdo  -> Retorna o jogador para um checkpoint no mapa.  


# EQUIPE SCHNEIDER'S LAST DANCE

Henrique 'Sdds Unity' Tsuneda:  Tech Artist 

Ian 'Deus da Godot' Duarte de Aguiar:  Programação

Lucas 'Big Day' dos Santos: Artista3D, MEGAnimador e Tech Artist

Pedro 'Poppy  tá solta' Machado: Coringa (Artista 2D, Som, Level Design, Monitor)

Jogo feito para a 14a Game Jam UNIVALI.

# Créditos

'Summer' - Música de Calvin Harris.
'Dark Fantasy' - Música de Kanye West.

NASA  ; Imagens da cutscene inicial. 

One Piece : Eiichiro 'G.' Oda.

## Texturas Utilizadas

  - https://freepbr.com/materials/rock-vertical-streaks-pbr-material/

 - https://freepbr.com/materials/angled-shale-cliff/

## Arte de background gerada usando:

  - https://skybox.blockadelabs.com/
