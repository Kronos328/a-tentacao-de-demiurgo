extends Area3D

@onready 
var spawn_marker = $SpawnMarker

func _ready():
	$MeshInstance3D.queue_free()


func _on_body_entered(body):
	if not body is CharacterBody3D:
		return
	
	body.register_failsafe_checkpoint(spawn_marker)
	set_collision_layer_value(1, false)
	set_collision_mask_value(1, false)
