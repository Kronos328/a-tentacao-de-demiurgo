extends Node3D

@onready var animation_player = $AnimationPlayer

func _ready():
	var time_to_wait = randf_range(1, 3)
	await get_tree().create_timer(time_to_wait).timeout
	animation_player.play("push")
	
