extends Node3D

@export
var player: CharacterBody3D 

@export
var offset_to_player: float

var largest_y_position: float

func _process(delta):
	if largest_y_position == null or (player.global_position.y - offset_to_player) > largest_y_position:
		global_position.y = player.global_position.y - offset_to_player
		largest_y_position = global_position.y
		


func _on_tp_back_area_body_entered(body):
	if not body is CharacterBody3D:
		return
	
	body.vortex_effect()	
