extends Area3D


func _on_body_entered(body):
	if not body is CharacterBody3D:
		return
	body.end()
	$MeshInstance3D.hide()
