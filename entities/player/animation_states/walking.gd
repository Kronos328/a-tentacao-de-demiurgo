extends AnimationState

func _update(delta):
	
	if target.velocity.is_equal_approx(Vector3.ZERO):
		get_root().change_state_name("Idle")
	elif target.sprinting:
		get_root().change_state_name("Running")
