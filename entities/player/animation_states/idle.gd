extends AnimationState


func _update(delta):
	
	if target.velocity != Vector3.ZERO and target.sprinting:
		get_root().change_state_name("Running")
	elif target.velocity != Vector3.ZERO:
		get_root().change_state_name("Walking")
