extends State


func update(delta: float):
	
	if Input.is_action_just_pressed("snap1") and target.is_on_floor():
		get_root().change_state_name("Snap1")
	elif Input.is_action_just_pressed("snap2"):
		get_root().change_state_name("Snap2")
