extends CharacterBody3D

signal snap_triggered

@export var go_to_marker: Node3D

@export var base_speed = 5
@export var sprint_speed = 8
@export var jump_velocity = 4
@export var sensitivity = 0.1
@export var accel = 10
var speed = base_speed
var sprinting = false
var camera_fov_extents = [75.0, 85.0] #index 0 is normal, index 1 is sprinting

var last_failsafe_checkpoint_marker: Marker3D

@onready 
var snap_state_machine: State = $SnapStateMachine

var can_run = true

@onready 
var viewmodel_animations = $ViewmodelAnimations

@onready 
var player_animations = $PlayerAnimations

var animating_viewmodel_for_snap := false

@export var animating_failsafe := false

@onready var parts = {
	"head": $head,
	"camera": $head/Personagem_First_Person_2/rig/Skeleton3D/BoneAttachment3D/WorldCamera,
}
@onready var world = get_parent()

var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")


const MIN_DISSOLVE_AMOUNT = 0
const MAX_DISSOLVE_AMOUNT = 1.4
var dissolve_material := preload("res://Tsuneda/VFX/materials/dissolve_material_corrigido.tres")

func _ready():
	snap_triggered.connect(PlayerDataManager._on_snap_trigger)
	snap_triggered.connect(lower_dissolve)
	world.pause.connect(_on_pause)
	world.unpause.connect(_on_unpause)
	
	parts.camera.current = true
	PlayerDataManager.reset_times_triggered_power()

func _process(delta):
	if Input.is_action_pressed("move_sprint") and can_run:
		sprinting = true
		speed = sprint_speed
	else:
		sprinting = false
		speed = base_speed

func _physics_process(delta):
	
	if animating_failsafe:
		velocity = Vector3.ZERO
		return
	
	if not is_on_floor():
		velocity.y -= gravity * delta

	if Input.is_action_just_pressed("move_jump") and is_on_floor():
		velocity.y += jump_velocity

	var input_dir = Input.get_vector("move_left", "move_right", "move_forward", "move_backward")
	var direction = input_dir.normalized().rotated(-parts.head.rotation.y)
	direction = Vector3(direction.x, 0, direction.y)
	if is_on_floor(): #don't lerp y movement
		velocity.x = lerp(velocity.x, direction.x * speed, accel * delta)
		velocity.z = lerp(velocity.z, direction.z * speed, accel * delta)
	
	_select_movement_animation(Vector2(direction.x, direction.y))

	move_and_slide()

func _input(event):
	if event is InputEventMouseMotion:
		if !world.paused:
			parts.head.rotation_degrees.y -= event.relative.x * sensitivity
			parts.head.rotation_degrees.x -= event.relative.y * sensitivity
			parts.head.rotation.x = clamp(parts.head.rotation.x, deg_to_rad(-90), deg_to_rad(90))

func _on_pause():
	pass

func _on_unpause():
	pass


func snap_1():
	go_to_marker.global_position = global_position
	snap_triggered.emit()


func snap_2():
	global_position = go_to_marker.global_position
	snap_triggered.emit()


func snap_2_failsafe():
	global_position = last_failsafe_checkpoint_marker.global_position
	snap_triggered.emit()


func register_failsafe_checkpoint(checkpoint_marker: Marker3D):
	last_failsafe_checkpoint_marker = checkpoint_marker
	go_to_marker.global_position = checkpoint_marker.global_position
	print("checkpoint registered")


func vortex_effect():
	snap_state_machine.change_state_name("Snap2Failsafe", true)


func set_can_run(value: bool):
	can_run = value


func _select_movement_animation(direction: Vector2):
	if animating_viewmodel_for_snap:
		return
	
	if direction.is_equal_approx(Vector2.ZERO) and viewmodel_animations.current_animation != "Idle":
		viewmodel_animations.play("Idle")
	elif direction != Vector2.ZERO and sprinting and viewmodel_animations.current_animation != "Running":
		viewmodel_animations.play("Running")
	elif direction != Vector2.ZERO and not sprinting and viewmodel_animations.current_animation != "Walking":
		viewmodel_animations.play("Walking")


func set_animating_viewmodel_for_snap(value: bool):
	animating_viewmodel_for_snap = value


func change_to_gameover():
	get_tree().change_scene_to_file("res://scenes/bad_end/bad_end.tscn")


func change_to_good_ending():
	get_tree().change_scene_to_file("res://scenes/good_end/good_end.tscn")


func end():
	$SnapStateMachine.change_state_name("GameGoodEnding")


func lower_dissolve():
	var a = MAX_DISSOLVE_AMOUNT / PlayerDataManager.MAX_ALLOWED_SNAPS
	dissolve_material.set_shader_parameter("dissolve_amount", (a * PlayerDataManager.times_triggered_power) / 2)


func reset_material():
	dissolve_material.set_shader_parameter("dissolve_amount", MIN_DISSOLVE_AMOUNT)
