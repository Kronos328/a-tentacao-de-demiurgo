extends Node

var children_amount = get_children().size()


func play():
	var rand = randi_range(0, children_amount-1)
	get_child(rand).play()
