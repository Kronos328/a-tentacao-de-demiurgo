extends State

func _enter():
	PlayerDataManager.game_over.connect(
		func(): change_state_name("GameOver")
	)
