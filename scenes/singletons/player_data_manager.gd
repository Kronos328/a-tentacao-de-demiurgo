extends Node

signal game_over

const PLAYER_NORMAL_WORLD_COLLISION_LAYER = 0
const PLAYER_CHAOS_WORLD_COLLISION_LAYER = 1

const MAX_ALLOWED_SNAPS = 5

var times_triggered_power := 0

var high_graphics := false

func _on_snap_trigger():
	times_triggered_power += 1
	print("Current snaps: %s" % times_triggered_power)
	if times_triggered_power > MAX_ALLOWED_SNAPS:
		game_over.emit()


func reset_times_triggered_power():
	times_triggered_power = 0
