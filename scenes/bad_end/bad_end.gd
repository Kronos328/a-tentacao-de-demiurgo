extends Control

func _ready():
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE


func _on_try_again_button_pressed():
	get_tree().change_scene_to_file("res://scenes/world.tscn")


func _on_give_up_button_pressed():
	get_tree().change_scene_to_file("res://scenes/main_menu/main_menu.tscn")


func _on_video_stream_player_finished():
	$VideoStreamPlayer.queue_free()
